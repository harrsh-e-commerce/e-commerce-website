import { Suspense } from 'react';

const lazyLoad = (LazyElement: React.LazyExoticComponent<() => JSX.Element>) => {
    return (
        <Suspense fallback="Loading...">
            <LazyElement />
        </Suspense>
    );
};

export default lazyLoad;
