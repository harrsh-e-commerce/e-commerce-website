import React from 'react';
import { createBrowserRouter } from 'react-router-dom';
import lazyLoad from '../libs/lazyLoad';
import { ProtectedRoute } from './ProtectedRoute';
import { PublicRoute } from './PublicRoute';

// Private routes
const Home = React.lazy(() => import('../pages/private/Home'));

// Public routes
const SignIn = React.lazy(() => import('../pages/public/SignIn'));

const routes = createBrowserRouter([
    {
        path: '/',
        element: <ProtectedRoute>{lazyLoad(Home)}</ProtectedRoute>
    },
    {
        path: '/signin',
        element: <PublicRoute>{lazyLoad(SignIn)}</PublicRoute>
    }
]);

export default routes;
