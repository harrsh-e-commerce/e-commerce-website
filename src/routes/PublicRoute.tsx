import { Navigate } from 'react-router-dom';
import { getUserToken } from '../utils/localToken';

export const PublicRoute = ({ children }: { children: JSX.Element }) => {
    const token = getUserToken();

    if (token) {
        return <Navigate to="/dashboard" replace={true} />;
    }

    return children;
};
