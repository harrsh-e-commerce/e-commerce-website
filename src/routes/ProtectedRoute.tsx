import { Navigate, useLocation } from 'react-router-dom';
import { getUserToken } from '../utils/localToken';

export const ProtectedRoute = ({ children }: { children: JSX.Element }) => {
    const token = getUserToken();
    const location = useLocation();

    if (!token) {
        return <Navigate to={`/signin?redirect=${location.pathname}`} replace={true} />;
    }

    return children;
};
