import env from '../env';

export const getUserToken = () => {
    return localStorage.getItem(env.app.token_id);
};

export const setUserToken = (token: string = '') => {
    localStorage.setItem(env.app.token_id, token);
};
